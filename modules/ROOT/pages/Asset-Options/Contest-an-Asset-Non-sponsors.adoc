Contest an Asset (Non-sponsors)


You can contest an asset whether or not you are a sponsor of the asset. If an Accepted asset is contested, it is moved back to Candidate status and must be re-endorsed by the asset's sponsors. All Sponsors and Assignees are notified by email, depending on their user settings, when the asset is contested. Refer to link:Sponsor-an-Asset.adoc[Sponsor an Asset] for more information.

To contest an asset:

1.  Click the drop-down menu for the asset.
+
image:Resources/Images/contest_asset.png[image]
2.  Select the *Contest* option.
3.  Enter a comment indicating why you are contesting the asset.
+
*NOTE*: A comment is required.
+
image:Resources/Images/contest_asset_comment.png[image]
4.  Click *Done*.
