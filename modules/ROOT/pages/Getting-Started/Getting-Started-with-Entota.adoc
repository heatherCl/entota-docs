Getting Started with Entota™


You can implement information governance using Entota™ following one of these methods:

* link:#Strategi[Strategic (Top-Down) Approach]
* link:#Rule-Bas[Rule-Based (Bottom-Up) Approach]
* link:#Critical[Critical Data Element (CDE) Approach]

[[Strategi]] Strategic (Top-Down) Approach
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

With the top-down approach, begin by identifying the organization’s link:Strategy.adoc[strategy] - in terms of its vision, mission, goals, and initiatives. When IT and the business staff can effectively communicate strategy across boundaries, the entire enterprise can begin to leverage data as a corporate asset. As you document the organization’s strategy, begin building link:Relationships.adoc[relationships] to the organization’s critical assets. The defining of critical assets then contributes to the creation of a link:Set-Up-Terms.adoc[business glossary], link:Data-Catalog.adoc[data catalog], and a comprehensive set of business link:Set-Up-Policies.adoc[policies] and enforceable link:Set-Up-Rules.adoc[rules]. In essence, the top-down approach starts with the “big picture” and trickles down to the fine details. This approach is useful in answering the questions “Why are we doing this?” and “How does this fit in our budget?”

This approach is recommended for governance teams with business stakeholder involvement from the get-go. It’s critical that the business strategy is documented as perceived enterprise-wide. Teams with doubts surrounding the enterprise’s strategy should consider deferring this effort to a later phase of implementation.

[[Rule-Bas]] Rule-Based (Bottom-Up) Approach
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When there’s an existing data quality, MDM, or other data management effort in place, organizations can initiate governance by documenting the link:Set-Up-Rules.adoc[rules] applied to specific link:Set-Up-a-Data-Set.adoc[data sets] in Entota™, centrify the link:Set-Up-a-System.adoc[systems] where those data sets come from, the business link:Set-Up-Policies.adoc[policies] that necessitate the link:Set-Up-Rules.adoc[rules], and the link:Set-Up-Terms.adoc[terms] that must be understood to understand the implications of each asset. Typically a collection of business policies also reveals link:Add-Initiatives.adoc[initiatives] and link:Add-Goals.adoc[goals] set at the enterprise and program levels.

If the governance effort is initiated by the IT team, it’s usually easier to begin where there are existing data management efforts - which is typically at the enforcement stage. This approach is best suited for customers already using BackOffice solutions such as dspMonitor™, dspConduct™, or other enforcement technologies.

[[Critical]] Critical Assets Approach
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A problem that many of our customers face is that governance is left solely to IT - the business staff aren’t interested in getting involved. This approach focuses on assets which are central to business operations or reporting - we call these “critical assets”. We document the link:Set-Up-a-System.adoc[systems] and link:Set-Up-a-Data-Set.adoc[data sets] containing these assets, assign each asset a business link:Set-Up-Terms.adoc[definition], and then document governing link:Set-Up-Policies.adoc[policies] and link:Set-Up-Rules.adoc[rules]. This enables IT to show value in the governance program while educating the business staff on the processes and technology involved.

This approach is great for governance teams still in the initial phase of information management and governance. It can provide quick value in a relatively short time frame which is sometimes crucial for maintaining a governance project.

No entry point to the Entota™ is better than another - it all depends on the organization’s maturity level and specific data governance environment. Each Entota™ customer has a designated Customer Success Manager who can help guide you, whichever approach you choose, and provide further expertise on implementations. To learn more about the Entota™ customer success program, please contact customersuccess@boaweb.com.
