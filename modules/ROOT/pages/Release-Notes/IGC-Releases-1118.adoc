[cols=",<",]
|===================================================================================================================================================================================================================================
|November 16, 2018 a|
*New Feature*

A new Enhanced Search page has been added to the IGC. The Enhanced Search provides an easy way to find and manage assets using the search bar filter, Jump To... links, and category filters. Category filters can query an asset’s:

* Asset type
* Endorsement status
* Assigned users
* Sponsors

The Enhanced Search provides multiple options for accessing strategy and non-strategy assets, and the category filters can help management-level users evaluate assets and their employees’ progress in managing them.

Refer to http://general.igchelp.com/Enhanced-Search.adoc[Enhanced Search] in the Online Help or https://youtu.be/s6zlLNI8TJs[watch the tutorial video.]

|===================================================================================================================================================================================================================================
