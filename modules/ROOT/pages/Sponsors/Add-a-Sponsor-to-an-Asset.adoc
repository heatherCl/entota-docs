Add a Sponsor to an Asset


You can add one or more sponsors to your asset. When you add a sponsor, the sponsor receives a notification in their notification panel and email, depending on their user settings.

To add or remove a sponsor:

1.  Click the *Sponsors* icon (image:Resources/Images/sponsors_icon.png[image]) on an asset’s detail page.
2.  Do one of the following:
* In the Sponsors selection panel, scroll to and click a person’s name to select that person as a sponsor.
* Start typing a name in the space provided, and then click a person’s name to select that person as a sponsor.
* Click the *x* next to the person’s name to remove them from the selection list.
+
image:Resources/Images/Sponsors_Panel.png[image]
3.  Click *OK* when you are done making selections to add or remove as sponsors of the asset.
4.  Click *Save* on the detail page.

Next, a sponsor can endorse, contest or edit an asset, refer to link:Sponsor-an-Asset.adoc[Sponsor an Asset] for more information.
