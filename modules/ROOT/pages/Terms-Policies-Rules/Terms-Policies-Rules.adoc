Terms, Policies, and Rules


You can add assets (Terms, Policies, and Rules) to your strategy to standardize, agree upon, and publicize the assets that inform your business objectives.

Learn about Terms, Policies, and Rules:

* link:Set-Up-Terms.adoc[Set Up Terms]
* link:Set-Up-Policies.adoc[Set Up Policies]
* link:Set-Up-Rules.adoc[Set Up Rules]
